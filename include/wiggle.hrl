-record(state, {
          audit = false :: boolean(),
          request_id = fifo_utils:uuid(),
          %% The callback module responsible for the REST interactions
          module :: module(),
          %% The path of the reqest
          path :: wiggle_h:path() | undefined,
          %% The method of the request, as binary and atom
          method :: binary() | undefined,
          method_a :: wiggle_h:method(),
          %% The API version
          version :: binary() | undefined,
          version_i :: pos_integer() | undefined,
          %% The token (either {token, ...} or user uuid)
          token,
          %% The object the reuqest is asking for (from the DB)
          obj,
          %% Body of the request (from the client)
          body :: maps:map(),
          %% When the request was started.
          start,
          %% The whole path as a binary
          path_bin :: binary() | undefined,
          %% The ETAG when generated
          etag,
          %% The bearer token when OAuth is used
          bearer,
          %% A cached set of permissons
          cached_perms,
          encoding,
          %% The permissions granted by the OAuth2 scope.
          %% If we don't have a scope aka don't use oatuh2 we always allow
          %% everything from a scope pov.
          scope_perms = [[<<"...">>]] :: [wiggle_h:permission()],
          %% Te full list header
          full_list = false :: boolean(),
          %% The full list fields
          full_list_fields = [] :: [binary()],
          %% Additional audit data gathered from the handler
          audit_data = #{}
         }).

-define(P(State), State#state.path_bin).
-define(MEx(Path, Service, Start), io_lib:format("~p~p", [Path, Start])).
%%-define(MEx(Path, Service, Start),
%%        statman_histogram:record_value({Path, {ext, Service}}, Start - erlang:system_time(micro_seconds))).
-define(MSnarl(Path, Start), ?MEx(Path, <<"snarl">>, Start)).
-define(MSniffle(Path, Start), ?MEx(Path, <<"sniffle">>, Start)).
-define(MHowl(Path, Start), ?MEx(Path, <<"howl">>, Start)).
-define(M(Path, Start), ok).
-define(UUID(N), <<N:36/binary>>).

-define(DEBUG(Msg), ?DEBUG(Msg, [])).
-define(DEBUG(Msg, Args),
        lager:debug("[~s] "++ Msg, [State#state.request_id | Args])).

-define(INFO(Msg), ?INFO(Msg, [])).
-define(INFO(Msg, Args),
        lager:info("[~s] "++ Msg, [State#state.request_id | Args])).

-define(WARNING(Msg), ?WARNING(Msg, [])).
-define(WARNING(Msg, Args),
        lager:warning("[~s] "++ Msg, [State#state.request_id | Args])).

-define(ERROR(Msg), ?ERROR(Msg, [])).
-define(ERROR(Msg, Args),
        lager:error("[~s] "++ Msg, [State#state.request_id | Args])).

%-define(M(Path, Start), statman_histogram:record_value({Path, total}, Start)).
%-define(M(Path, Start), statman_histogram:record_value({Path, total}, Start)).

-define(V3, <<"3">>).

-define(V, ?V3).
-define(CHUNK_SIZE, 5242880).
