%% Feel free to use, reuse and abuse the code in this file.

%% @doc Hello world handler.
-module(wiggle_dataset_h).

-include("wiggle.hrl").
-define(CACHE, dataset).
-define(LIST_CACHE, dataset_list).
-define(FULL_CACHE, dataset_full_list).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

-behaviour(wiggle_rest_h).

-export([allowed_methods/3,
         init/1,
         get/1,
         permission_required/2,
         read/2,
         create/3,
         write/3,
         delete/2,
         content_types_accepted/1,
         content_types_provided/1]).

-define(WRETRY, 5).

init(S = #state{path = [?UUID(Dataset)| _]}) ->
    wiggle_audit:set(<<"dataset">>, Dataset, S);

init(S) ->
    S.

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>, <<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Dataset)]) ->
    [<<"GET">>, <<"DELETE">>, <<"PUT">>];

allowed_methods(_Version, _Token, [?UUID(_Dataset), <<"dataset.gz">>]) ->
    [<<"PUT">>, <<"GET">>];

allowed_methods(_Version, _Token, [?UUID(_Dataset), <<"dataset.bz2">>]) ->
    [<<"PUT">>, <<"GET">>];

allowed_methods(_, _Token, [?UUID(_Dataset), <<"networks">>, _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Dataset), <<"metadata">>|_]) ->
    [<<"PUT">>, <<"DELETE">>].

get(State = #state{path = [?UUID(Dataset) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, dataset_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Dataset, TTL1, TTL2, not_found,
                  fun() -> ls_dataset:get(Dataset) end);
            _ ->
                ls_dataset:get(Dataset)
        end,
    ?MSniffle(?P(State), Start),
    R;

get(_State) ->
    not_found.

permission_required(get, []) ->
    {ok, [<<"cloud">>, <<"datasets">>, <<"list">>]};

permission_required(post, []) ->
    {ok, [<<"cloud">>, <<"datasets">>, <<"create">>]};

permission_required(get, [?UUID(Dataset)]) ->
    {ok, [<<"datasets">>, Dataset, <<"get">>]};

permission_required(put, [?UUID(Dataset)]) ->
    {ok, [<<"datasets">>, Dataset, <<"edit">>]};

permission_required(post, [?UUID(Dataset)]) ->
    {ok, [<<"datasets">>, Dataset, <<"create">>]};

permission_required(get, [?UUID(Dataset), <<"dataset.gz">>]) ->
    {ok, [<<"datasets">>, Dataset, <<"export">>]};

permission_required(put, [?UUID(Dataset), <<"dataset.gz">>]) ->
    {ok, [<<"datasets">>, Dataset, <<"create">>]};

permission_required(delete, [?UUID(Dataset)]) ->
    {ok, [<<"datasets">>, Dataset, <<"delete">>]};

permission_required(put, [?UUID(Dataset), <<"networks">>, _]) ->
    {ok, [<<"datasets">>, Dataset, <<"edit">>]};

permission_required(delete, [?UUID(Dataset), <<"networks">>, _]) ->
    {ok, [<<"datasets">>, Dataset, <<"edit">>]};

permission_required(put, [?UUID(Dataset), <<"metadata">> | _]) ->
    {ok, [<<"datasets">>, Dataset, <<"edit">>]};

permission_required(delete, [?UUID(Dataset), <<"metadata">> | _]) ->
    {ok, [<<"datasets">>, Dataset, <<"edit">>]};

permission_required(_Method, _Path) ->
    undefined.

content_types_accepted(
  #state{method = <<"PUT">>, path=[_, <<"dataset.gz">>]}) ->
    [{{<<"application">>, <<"x-gzip">>, '*'}, write_raw}];
content_types_accepted(_) ->
    wiggle_h:accepted().

content_types_provided(
  #state{method = <<"GET">>, path=[_, <<"dataset.gz">>]}) ->
    [{{<<"application">>, <<"x-gzip">>, []}, read_raw}];
content_types_provided(_) ->
    wiggle_h:provided().

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------

read(Req, State = #state{path = []}) ->
    case cowboy_req:qs_val(<<"avail">>, Req) of
        {undefined, Req1} ->
            wiggle_h:list(<<"datasets">>,
                          fun ls_dataset:stream/3,
                          fun ft_dataset:uuid/1,
                          fun ft_dataset:to_json/1,
                          Req1, State);
        {_, Req1} ->
            StreamFn = fun (_, Fn, Acc) ->
                               %% Ugh we expect a slighty different format
                               Fn1 = fun ({data, X}, AccIn) ->
                                             X1 = [{dummy, E} || E <- X],
                                             Fn({data, X1}, AccIn);
                                         (Other, AccIn) ->
                                             Fn(Other, AccIn)
                                     end,
                               ls_dataset:available(Fn1, Acc)
                       end,
            UUIDFn = fun(#{<<"uuid">> := UUID}) -> UUID end,
            IdentityFn = fun(X) -> X end,
            wiggle_h:list(<<"datasets">>,
                          StreamFn,
                          UUIDFn,
                          IdentityFn,
                          Req1, State)
    end;

read(Req, State = #state{path = [?UUID(_Dataset)], obj = Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"get">>, State),
    {ft_dataset:to_json(Obj), Req, State1};

read(Req, State = #state{path = [UUID, <<"dataset.gz">>], obj = _Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"download">>, State),
    {ok, {Host, Port, AKey, SKey, Bucket}} = libsniffle:s3(image),
    Config = [{host, Host},
              {port, Port},
              {chunk_size, ?CHUNK_SIZE},
              {bucket, Bucket},
              {access_key, AKey},
              {secret_key, SKey}],
    {ok, D} = fifo_s3_download:new(UUID, Config),
    StreamFun = fun(SendChunk) ->
                        do_stream(SendChunk, D)
                end,
    {{chunked, StreamFun}, Req, State1}.

%%--------------------------------------------------------------------
%% POST
%%--------------------------------------------------------------------

create(Req, State = #state{path = [UUID], version = Version}, Decoded) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"import_manifest">>, State),
    case ls_dataset:create(UUID) of
        duplicate ->
            State2 = wiggle_audit:fail("duplicate", State1),
            {false, Req, State2};
        _ ->
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            import_manifest(UUID, Decoded),
            ls_dataset:imported(UUID, 0),
            ls_dataset:status(UUID, <<"pending">>),
            {{true, <<"/api/", Version/binary, "/datasets/", UUID/binary>>},
             Req, State1#state{body = Decoded}}
    end;

create(Req, State = #state{path = [], version = Version}, Decoded) ->
    e2qc:teardown(?LIST_CACHE),
    e2qc:teardown(?FULL_CACHE),
    case Decoded of
        #{<<"url">> := URL} ->
            Start = erlang:system_time(micro_seconds),
            {ok, UUID} = ls_dataset:import(URL),
            State1 =
                wiggle_audit:sets([{<<"action">>, <<"import_url">>},
                                   {<<"url">>, URL},
                                   {<<"dataset">>, UUID}], State),
            ?MSniffle(?P(State1), Start),
            {{true, <<"/api/", Version/binary, "/datasets/", UUID/binary>>},
             Req, State1#state{body = Decoded}};
        #{<<"config">>   := Config,
          <<"snapshot">> := Snap,
          <<"vm">>       := Vm} ->
            Start1 = erlang:system_time(micro_seconds),
            {ok, UUID} = ls_vm:promote_snapshot(Vm, Snap, Config),
            State1 =
                wiggle_audit:sets([{<<"action">>, <<"import_vm">>},
                                   {<<"vm">>, Vm},
                                   {<<"snapshot">>, Snap},
                                   {<<"dataset">>, UUID}], State),
            ?MSniffle(?P(State1), Start1),
            {{true, <<"/api/", Version/binary, "/datasets/", UUID/binary>>},
             Req, State1#state{body = Decoded}}
    end.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

write(Req, State = #state{path = [UUID]}, Decoded) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"write">>, State),
    case ls_dataset:create(UUID) of
        duplicate ->
            State2 = wiggle_audit:fail("duplicate", State1),
            {false, Req, State2};
        _ ->
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            import_manifest(UUID, Decoded),
            ls_dataset:imported(UUID, 0),
            ls_dataset:status(UUID, <<"pending">>),
            {true, Req, State1#state{body = Decoded}}
    end;

write(Req, State = #state{path = [UUID, <<"dataset.gz">>]}, _) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"write_dataset">>, State),
    case ls_dataset:get(UUID) of
        {ok, R} ->
            Size = ft_dataset:image_size(R),
            ls_dataset:status(UUID, <<"importing">>),
            {Res, Req1} = import_dataset(UUID, 0, ensure_integer(Size), Req),
            {Res, Req1, State1};
        E ->
            State2 = wiggle_audit:fail(E, State1),
            {false, Req, State2}
    end;

write(Req, State = #state{path = [?UUID(Dataset), <<"metadata">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    ok = ls_dataset:set_metadata(Dataset, [{Path ++ [K], V}]),
    e2qc:evict(?CACHE, Dataset),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State};

write(Req, State = #state{path = [?UUID(Dataset), <<"networks">>, NIC],
                          obj = Obj},
      #{<<"description">> := Desc}) ->
    State1 = wiggle_audit:sets([{<<"action">>, <<"add_nic">>},
                                {<<"nic">>, NIC},
                                {<<"desc">>, Desc}], State),
    Start = erlang:system_time(micro_seconds),
    [ls_dataset:remove_network(Dataset, E) ||
        E = {NICe, _} <- ft_dataset:networks(Obj), NICe =:= NIC],
    Reply = ls_dataset:add_network(Dataset, {NIC, Desc}),
    e2qc:evict(?CACHE, Dataset),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.

%%--------------------------------------------------------------------
%% DELETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Dataset), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_dataset:set_metadata(Dataset, [{Path, delete}]),
    e2qc:evict(?CACHE, Dataset),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {Reply =:= ok, Req, State};

delete(Req, State = #state{path = [?UUID(Dataset), <<"networks">>, Nic]}) ->
    State1 = wiggle_audit:sets([{<<"action">>, <<"remove_nic">>},
                                {<<"nic">>, Nic}], State),
    Start = erlang:system_time(micro_seconds),
    {ok, D} = ls_dataset:get(Dataset),
    Ns = [N || N = {ANic, _} <- ft_dataset:networks(D), ANic =:= Nic],
    case Ns of
        [N] ->
            ok = ls_dataset:remove_network(Dataset, N),
            e2qc:evict(?CACHE, Dataset),
            e2qc:teardown(?FULL_CACHE),
            ?MSniffle(?P(State), Start),
            {true, Req, State1};
        _ ->
            State2 = wiggle_audit:fail("not_found", State1),
            {false, Req, State2}
    end;


delete(Req, State = #state{path = [?UUID(Dataset)]}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"delete">>, State),
    Start = erlang:system_time(micro_seconds),
    case ls_dataset:get(Dataset) of
        {ok, D} ->
            case ft_dataset:status(D) of
                <<"importing">> ->
                    ?MSniffle(?P(State), Start),
                    State2 = wiggle_audit:fail("importing", State1),
                    {409, Req, State2};
                _ ->
                    Reply = case ls_dataset:delete(Dataset) of
                                {error, E} ->
                                    E;
                                _ ->
                                    ok
                            end,
                    e2qc:evict(?CACHE, Dataset),
                    e2qc:teardown(?LIST_CACHE),
                    e2qc:teardown(?FULL_CACHE),
                    ?MSniffle(?P(State), Start),
                    State2 = wiggle_audit:maybe_fail(Reply, State1),
                    {Reply =:= ok, Req, State2}
                end;
        E ->
            State2 = wiggle_audit:fail(E, State1),
            {404, Req, State2}
    end.

%%--------------------------------------------------------------------
%% Internal
%%--------------------------------------------------------------------

do_import([], _UUID, _O) ->
    ok;

do_import([{K, F} | R], UUID, O) ->
    case jsxd:get(K, O) of
        {ok, V}  ->
            F(UUID, V);
        _ ->
            ok
    end,
    do_import(R, UUID, O).

import_manifest(UUID, D1) ->
    D2 = maps:to_list(D1),
    do_import(
      [
       {<<"metadata">>, fun ls_dataset:set_metadata/2},
       {<<"name">>, fun ls_dataset:name/2},
       {<<"version">>, fun ls_dataset:version/2},
       {<<"description">>, fun ls_dataset:description/2},
       {<<"disk_driver">>, fun ls_dataset:disk_driver/2},
       {<<"nic_driver">>, fun ls_dataset:nic_driver/2},
       {<<"users">>, fun ls_dataset:users/2},
       {[<<"tags">>, <<"kernel_version">>], fun ls_dataset:kernel_version/2}
      ], UUID, D2),
    ls_dataset:image_size(
      UUID,
      ensure_integer(
        jsxd:get(<<"image_size">>,
                 jsxd:get([<<"files">>, 0, <<"size">>], 0, D1), D1))),
    RS = jsxd:get(<<"requirements">>, [], D1),
    Networks = jsxd:get(<<"networks">>, [], RS),
    [ls_dataset:add_network(UUID, {NName, NDesc}) ||
        #{<<"description">> := NDesc, <<"name">> := NName} <- Networks],
    case jsxd:get(<<"homepage">>, D1) of
        {ok, HomePage} ->
            ls_dataset:set_metadata(
              UUID,
              [{<<"homepage">>, HomePage}]);
        _ ->
            ok
    end,
    case jsxd:get(<<"min_platform">>, RS) of
        {ok, Min} ->
            Min1 = [V || {_, V} <- Min],
            [M | _] = lists:sort(Min1),
            R = {must, '>=', <<"sysinfo.Live Image">>, M},
            ls_dataset:add_requirement(UUID, R);
        _ ->
            ok
    end,
    case jsxd:get(<<"os">>, D1) of
        {ok, <<"smartos">>} ->
            ls_dataset:os(UUID, <<"smartos">>),
            ls_dataset:type(UUID, zone);
        {ok, OS} ->
            case jsxd:get(<<"type">>, D1) of
                {ok, <<"lx-dataset">>} ->
                    ls_dataset:os(UUID, OS),
                    ls_dataset:type(UUID, zone),
                    ls_dataset:zone_type(UUID, lx);
                _ ->
                    ls_dataset:os(UUID, OS),
                    ls_dataset:type(UUID, kvm)
            end
    end.

import_dataset(UUID, Idx, TotalSize, Req) ->
    {ok, {Host, Port, AKey, SKey, Bucket}} = libsniffle:s3(image),

    ChunkSize = ?CHUNK_SIZE,
    {ok, Upload} = fifo_s3_upload:new(AKey, SKey, Host, Port,
                                      Bucket, UUID),
    Ctx = crypto:hash_init(sha),
    import_dataset_s3(UUID, Idx, TotalSize, {more, Req},
                      ChunkSize, Upload, Ctx, <<>>).

import_dataset_s3(UUID, Idx, TotalSize, {State, Req}, ChunkSize, Upload, Ctx,
                  Acc)
  when byte_size(Acc) >= ChunkSize ->
    <<Chunk:ChunkSize/binary, Acc1/binary>> = Acc,
    case fifo_s3_upload:part(Upload, binary:copy(Chunk)) of
        ok ->
            Idx1 = Idx + 1,
            Done = (Idx1 * ChunkSize) / TotalSize,
            ls_dataset:imported(UUID, Done),
            SR = {State, Req},
            import_dataset_s3(UUID, Idx1, TotalSize, SR, ChunkSize, Upload,
                              Ctx, Acc1);
        {error, E} ->
            fifo_s3_upload:abort(Upload),
            lager:error("Upload error: ~p", [E]),
            ls_dataset:status(UUID, <<"failed">>),
            {false, Req}

    end;

import_dataset_s3(UUID, _Idx, _TotalSize, {ok, Req},
                  _ChunkSize, Upload, Ctx, Acc) ->
    case fifo_s3_upload:part(Upload, binary:copy(Acc)) of
        ok ->
            fifo_s3_upload:done(Upload),
            ls_dataset:imported(UUID, 1),

            {ok, D} = ls_dataset:get(UUID),
            SHA1 = ft_dataset:sha1(D),
            case base16:encode(crypto:hash_final(Ctx)) of
                Digest when Digest == SHA1 ->
                    ls_dataset:status(UUID, <<"imported">>),
                    {true, Req};
                Digest when SHA1 == <<>> ->
                    ls_dataset:sha1(UUID, Digest),
                    ls_dataset:status(UUID, <<"imported">>),
                    {true, Req};
                Digest ->
                    ls_dataset:sha1(UUID, Digest),
                    ls_dataset:status(UUID, <<"imported">>),
                    {false, Req}
            end;
        {error, E} ->
            fifo_s3_upload:abort(Upload),
            lager:error("Upload error: ~p", [E]),
            ls_dataset:status(UUID, <<"failed">>),
            {false, Req}
    end;


import_dataset_s3(UUID, Idx, TotalSize, {more, Req}, ChunkSize, Upload, Ctx,
                  Acc) ->
    {State, Data, Req1} = cowboy_req:body(Req, []),
    Ctx1 = crypto:hash_update(Ctx, Data),
    Acc1 = <<Acc/binary, Data/binary>>,
    SR = {State, Req1},
    import_dataset_s3(UUID, Idx, TotalSize, SR, ChunkSize, Upload, Ctx1, Acc1).

ensure_integer(I) when is_integer(I) ->
    I;
ensure_integer(L) when is_list(L) ->
    list_to_integer(L);
ensure_integer(B) when is_binary(B) ->
    binary_to_integer(B).

do_stream(SendChunk, D) ->
    case fifo_s3_download:get(D) of
        {ok, done} ->
            ok;
        {ok, Data} ->
            SendChunk(binary:copy(Data)),
            do_stream(SendChunk, D)
    end.
