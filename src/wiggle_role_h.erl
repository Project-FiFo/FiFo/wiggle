-module(wiggle_role_h).
-include("wiggle.hrl").

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

-define(CACHE, role).
-define(LIST_CACHE, role_list).
-define(FULL_CACHE, role_full_list).

-export([allowed_methods/3,
         init/1,
         get/1,
         permission_required/2,
         read/2,
         create/3,
         write/3,
         delete/2]).

-behaviour(wiggle_rest_h).

init(S = #state{path = [?UUID(Role)| _]}) ->
    wiggle_audit:set(<<"role">>, Role, S);

init(S) ->
    S.

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>, <<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Role)]) ->
    [<<"GET">>, <<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Role), <<"metadata">> | _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Role), <<"permissions">>
                                       | _Permission]) ->
    [<<"PUT">>, <<"DELETE">>].

get(State = #state{path = [?UUID(Role), <<"permissions">> | Permission]}) ->
    case {Permission, wiggle_role_h:get(State#state{path = [?UUID(Role)]})} of
        {_, not_found} ->
            not_found;
        {[], {ok, Obj}} ->
            {ok, Obj};
        {P, {ok, Obj}} ->
            case lists:member(P, ft_role:permissions(Obj)) of
                true ->
                    {ok, Obj};
                _ -> not_found
            end
    end;

get(State = #state{path = [?UUID(Role) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, role_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Role, TTL1, TTL2, not_found,
                  fun() -> ls_role:get(Role) end);
            _ ->
                ls_role:get(Role)
        end,
    ?MSnarl(?P(State), Start),
    R.

permission_required(get, []) ->
    {ok, [<<"cloud">>, <<"roles">>, <<"list">>]};

permission_required(post, []) ->
    {ok, [<<"cloud">>, <<"roles">>, <<"create">>]};

permission_required(get, [?UUID(Role)]) ->
    {ok, [<<"roles">>, Role, <<"get">>]};

permission_required(put, [?UUID(Role)]) ->
    {ok, [<<"roles">>, Role, <<"create">>]};

permission_required(delete, [?UUID(Role)]) ->
    {ok, [<<"roles">>, Role, <<"delete">>]};

permission_required(put, [?UUID(Role), <<"permissions">> | Permission]) ->
    {multiple, [[<<"roles">>, Role, <<"grant">>],
                [<<"permissions">>, Permission, <<"grant">>]]};

permission_required(delete, [?UUID(Role), <<"permissions">> | Permission]) ->
    {multiple, [[<<"roles">>, Role, <<"revoke">>],
                [<<"permissions">>, Permission, <<"revoke">>]]};

permission_required(put, [?UUID(Role), <<"metadata">> | _]) ->
    {ok, [<<"roles">>, Role, <<"edit">>]};

permission_required(delete, [?UUID(Role), <<"metadata">> | _]) ->
    {ok, [<<"roles">>, Role, <<"edit">>]};

permission_required(_Method, _Path) ->
    undefined.

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------

read(Req, State = #state{path = []}) ->
    wiggle_h:list(<<"roles">>,
                  fun ls_role:stream/3,
                  fun ft_role:uuid/1,
                  fun to_json/1,
                  Req, State);

read(Req, State = #state{path = [?UUID(_Role)], obj = RoleObj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"get">>, State),
    {to_json(RoleObj), Req, State1}.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

create(Req, State = #state{path = [], version = Version}, Decoded) ->
    {ok, Role} = jsxd:get(<<"name">>, Decoded),
    Start = erlang:system_time(micro_seconds),
    {ok, UUID} = ls_role:add(Role),
    e2qc:teardown(?LIST_CACHE),
    ?MSnarl(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"create">>},
                                {<<"role">>, UUID},
                                {<<"name">>, Role}], State),
    {{true, <<"/api/", Version/binary, "/roles/", UUID/binary>>},
     Req, State1#state{body = Decoded}}.

write(Req, State = #state{path = [?UUID(Role), <<"metadata">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Role),
    e2qc:teardown(?LIST_CACHE),
    ls_role:set_metadata(Role, [{[<<"public">> | Path] ++ [K], V}]),
    ?MSnarl(?P(State), Start),
    {true, Req, State};

write(Req, State = #state{path = [?UUID(Role), <<"permissions">> | Permission]},
      _Body) ->
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Role),
    e2qc:teardown(?LIST_CACHE),
    Reply = ls_role:grant(Role, Permission),
    ?MSnarl(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"grant">>},
                                {<<"permission">>, Permission}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.

%%--------------------------------------------------------------------
%% DEETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Role), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Role),
    e2qc:teardown(?LIST_CACHE),
    ls_role:set_metadata(Role, [{[<<"public">> | Path], delete}]),
    ?MSnarl(?P(State), Start),
    {true, Req, State};

delete(Req, State = #state{path = [?UUID(Role), <<"permissions">>
                                       | Permission]}) ->
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Role),
    e2qc:teardown(?LIST_CACHE),
    Reply = ls_role:revoke(Role, Permission),
    ?MSnarl(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"revoke">>},
                                {<<"permission">>, Permission}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req, State = #state{path = [?UUID(Role)]}) ->
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Role),
    e2qc:teardown(?LIST_CACHE),
    Reply = ls_role:delete(Role),
    ?MSnarl(?P(State), Start),
    State1 = wiggle_audit:sets(<<"action">>, <<"delete">>, State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.

%%--------------------------------------------------------------------
%% Internal Functions
%%--------------------------------------------------------------------

to_json(E) ->
    E1 = ft_role:to_json(E),
    jsxd:update([<<"metadata">>],
                fun(M) ->
                        jsxd:get([<<"public">>], #{}, M)
                end, #{}, E1).
