-module(wiggle_client_h).
-include("wiggle.hrl").

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

-define(CACHE, client).
-define(LIST_CACHE, client_list).
-define(FULL_CACHE, client_full_list).

-export([allowed_methods/3,
         init/1,
         get/1,
         permission_required/2,
         read/2,
         create/3,
         write/3,
         delete/2]).

-behaviour(wiggle_rest_h).

init(S = #state{path = [?UUID(Client)| _]}) ->
    wiggle_audit:set(<<"client">>, Client, S);

init(S) ->
    S.

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>, <<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Client)]) ->
    [<<"GET">>, <<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Client), <<"permissions">> | _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Client), <<"uris">>]) ->
    [<<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Client), <<"uris">>, _]) ->
    [<<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Client), <<"metadata">> | _]) ->
    [<<"PUT">>, <<"DELETE">>].

get(State = #state{path = [?UUID(Client) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, client_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Client, TTL1, TTL2, not_found,
                  fun() -> ls_client:get(Client) end);
            _ ->
                ls_client:get(Client)
        end,
    ?MSnarl(?P(State), Start),
    R.

permission_required(get, []) ->
    {ok, [<<"cloud">>, <<"clients">>, <<"list">>]};

permission_required(post, []) ->
    {ok, [<<"cloud">>, <<"clients">>, <<"create">>]};

permission_required(get, [?UUID(Client)]) ->
    {ok, [<<"clients">>, Client, <<"get">>]};

permission_required(put, [?UUID(Client)]) ->
    {ok, [<<"clients">>, Client, <<"edit">>]};

permission_required(delete, [?UUID(Client)]) ->
    {ok, [<<"clients">>, Client, <<"delete">>]};

permission_required(put, [?UUID(Client), <<"permissions">> | _]) ->
    {ok, [<<"clients">>, Client, <<"grant">>]};

permission_required(delete, [?UUID(Client), <<"permissions">> | _]) ->
    {ok, [<<"clients">>, Client, <<"revoke">>]};

permission_required(post, [?UUID(Client), <<"uris">>]) ->
    {ok, [<<"clients">>, Client, <<"edit">>]};

permission_required(delete, [?UUID(Client), <<"uris">>, _]) ->
    {ok, [<<"clients">>, Client, <<"edit">>]};

permission_required(put, [?UUID(Client), <<"metadata">> | _]) ->
    {ok, [<<"clients">>, Client, <<"edit">>]};

permission_required(delete, [?UUID(Client), <<"metadata">> | _]) ->
    {ok, [<<"clients">>, Client, <<"edit">>]};

permission_required(_Method, _Path) ->
    undefined.

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------

read(Req, State = #state{path = []}) ->
    wiggle_h:list(<<"clients">>,
                  fun ls_client:stream/3,
                  fun ft_client:uuid/1,
                  fun to_json/1,
                  Req, State);

read(Req, State = #state{path = [_Client], obj = ClientObj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"get">>, State),
    {to_json(ClientObj), Req, State1}.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

create(Req, State = #state{token = Token, path = [], version = Version},
       #{<<"client">> := Client,
         <<"secret">> := Secret}) ->
    {ok, Creator} = ls_user:get(Token),
    CUUID = ft_user:uuid(Creator),
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"action">>, <<"create">>},
                                {<<"user">>, CUUID},
                                {<<"name">>, Client}], State),
    case ls_client:add(CUUID, Client) of
        {ok, UUID} ->
            ?MSnarl(?P(State), Start),
            Start1 = erlang:system_time(micro_seconds),
            ok = ls_client:secret(UUID, Secret),
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            ?MSnarl(?P(State), Start1),
            State2 = wiggle_audit:set(<<"client">>, UUID, State1),
            {{true, <<"/api/", Version/binary, "/clients/", UUID/binary>>},
             Req, State2};
        duplicate ->
            ?MSniffle(?P(State), Start),
            {ok, Req1} = cowboy_req:reply(409, Req),
            State2 = wiggle_audit:fail("duplicate", State1),
            {halt, Req1, State2}
    end;

create(Req, State = #state{path = [?UUID(Client), <<"uris">>],
                           version = Version}, #{<<"uri">> := URI}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_client:uri_add(Client, URI),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"add_uri">>},
                                {<<"uri">>, URI}], State),
    {{true, <<"/api/", Version/binary, "/clients/", Client/binary>>},
     Req, State1}.

write(Req, State = #state{path =  [?UUID(Client)]},
      #{<<"secret">> := Secret}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_client:secret(Client, Secret),
    ?MSnarl(?P(State), Start),
    State1 = wiggle_audit:sets(<<"action">>, <<"set_secret">>, State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Client), <<"metadata">> | Path]}, O)
  when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    ok = ls_client:set_metadata(Client, [{[<<"public">> | Path] ++ [K],
                                          V}]),
    e2qc:evict(?CACHE, Client),
    e2qc:teardown(?FULL_CACHE),
    ?MSnarl(?P(State), Start),
    {true, Req, State}.

%%--------------------------------------------------------------------
%% DEETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Client), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_client:set_metadata(Client, [{[<<"public">> | Path], delete}]),
    e2qc:evict(?CACHE, Client),
    e2qc:teardown(?FULL_CACHE),
    ?MSnarl(?P(State), Start),
    {true, Req, State};

delete(Req, State = #state{path = [?UUID(Client), <<"uris">>, URIHash],
                           obj = Obj}) ->
    URIs = ft_client:uris(Obj),
    ID = binary_to_integer(URIHash),
    Found = lists:filter(fun(ThisURI) ->
                                 ID == erlang:phash2(ThisURI)
                         end, URIs),
    State1 = wiggle_audit:sets(<<"action">>, <<"delete_uri">> , State),
    case Found of
        [URI] ->
            Reply = ls_client:uri_remove(Client, URI),
            e2qc:evict(?CACHE, Client),
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            State2 = wiggle_audit:sets(<<"uri">>, URI, State),
            State3 = wiggle_audit:maybe_fail(Reply, State2),
            {Reply =:= ok, Req, State3};
        _ ->
            State2 = wiggle_audit:fail("not_found", State1),
            {true, Req, State2}
    end;

delete(Req, State = #state{path = [?UUID(Client)]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_client:delete(Client),
    e2qc:evict(?CACHE, Client),
    e2qc:teardown(?LIST_CACHE),
    e2qc:teardown(?FULL_CACHE),
    ?MSnarl(?P(State), Start),
    State1 = wiggle_audit:sets(<<"action">>, <<"delete">>, State),
    {true, Req, State1}.

%%--------------------------------------------------------------------
%% Internal Functions
%%--------------------------------------------------------------------

to_json(U) ->
    U1 = ft_client:to_json(U),
    U2 = jsxd:delete([<<"secret">>], U1),
    U3 = jsxd:update([<<"metadata">>],
                     fun(M) ->
                             jsxd:get([<<"public">>], #{}, M)
                     end, #{}, U2),
    jsxd:update([<<"redirect_uris">>],
                fun(URIs) ->
                        [{integer_to_binary(erlang:phash2(URI)), URI}
                         || URI <- URIs]
                end, #{}, U3).
