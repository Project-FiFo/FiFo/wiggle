%% Feel free to use, reuse and abuse the code in this file.

%% @doc Hello world handler.
-module(wiggle_network_h).
-include("wiggle.hrl").

-define(CACHE, network).
-define(LIST_CACHE, network_list).
-define(FULL_CACHE, network_full_list).

-export([allowed_methods/3,
         init/1,
         get/1,
         permission_required/2,
         read/2,
         create/3,
         write/3,
         delete/2]).

-behaviour(wiggle_rest_h).

init(S = #state{path = [?UUID(Network)| _]}) ->
    wiggle_audit:set(<<"network">>, Network, S);

init(S) ->
    S.

allowed_methods(_Version, _Token, [?UUID(_Network), <<"metadata">>|_]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Network), <<"ipranges">>, _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Network), <<"resolvers">>, _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>, <<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Network)]) ->
    [<<"GET">>, <<"PUT">>, <<"DELETE">>].

get(State = #state{path = [?UUID(Network) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, network_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Network, TTL1, TTL2, not_found,
                  fun() -> ls_network:get(Network) end);
            _ ->
                ls_network:get(Network)
        end,
    ?MSniffle(?P(State), Start),
    R;

get(_State) ->
    not_found.

permission_required(get, []) ->
    {ok, [<<"cloud">>, <<"networks">>, <<"list">>]};

permission_required(post, []) ->
    {ok, [<<"cloud">>, <<"networks">>, <<"create">>]};

permission_required(get, [?UUID(Network)]) ->
    {ok, [<<"networks">>, Network, <<"get">>]};

permission_required(delete, [?UUID(Network)]) ->
    {ok, [<<"networks">>, Network, <<"delete">>]};

permission_required(put, [?UUID(_Network)]) ->
    {ok, [<<"cloud">>, <<"networks">>, <<"create">>]};

permission_required(put, [?UUID(Network), <<"ipranges">>,  _]) ->
    {ok, [<<"networks">>, Network, <<"edit">>]};

permission_required(delete, [?UUID(Network), <<"ipranges">>, _]) ->
    {ok, [<<"networks">>, Network, <<"edit">>]};

permission_required(put, [?UUID(Network), <<"resolvers">>,  _]) ->
    {ok, [<<"networks">>, Network, <<"edit">>]};

permission_required(delete, [?UUID(Network), <<"resolvers">>, _]) ->
    {ok, [<<"networks">>, Network, <<"edit">>]};

permission_required(put, [?UUID(Network), <<"metadata">> | _]) ->
    {ok, [<<"networks">>, Network, <<"edit">>]};

permission_required(delete, [?UUID(Network), <<"metadata">> | _]) ->
    {ok, [<<"networks">>, Network, <<"edit">>]};

permission_required(_Method, _Path) ->
    undefined.

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------

read(Req, State = #state{path = []}) ->
    wiggle_h:list(<<"networks">>,
                  fun ls_network:stream/3,
                  fun ft_network:uuid/1,
                  fun ft_network:to_json/1,
                  Req, State);

read(Req, State = #state{path = [?UUID(_Network)], obj = Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"get">>, State),
    {ft_network:to_json(Obj), Req, State1}.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

create(Req, State = #state{path = [], version = Version}, Data) ->
    {ok, Network} = jsxd:get(<<"name">>, Data),
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"action">>, <<"create">>},
                                {<<"name">>,   Network}], State),
    case ls_network:create(Network) of
        {ok, UUID} ->
            ?MSniffle(?P(State), Start),
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            State2 = wiggle_audit:sets(<<"network">>, UUID, State1),
            {{true, <<"/api/", Version/binary, "/networks/", UUID/binary>>},
             Req, State2#state{body = Data}};
        duplicate ->
            ?MSniffle(?P(State), Start),
            {ok, Req1} = cowboy_req:reply(409, Req),
            State2 = wiggle_audit:fail("duplicate", State1),
            {halt, Req1, State2}
    end.

write(Req, State = #state{path = [?UUID(Network), <<"ipranges">>, IPrange]},
      _Data) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_network:add_iprange(Network, IPrange),
    ?MSniffle(?P(State), Start),
    e2qc:evict(?CACHE, Network),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"add_iprange">>},
                                {<<"iprange">>, IPrange}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Network), <<"resolvers">>, Resolver]},
      _Data) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_network:add_resolver(Network, Resolver),
    ?MSniffle(?P(State), Start),
    e2qc:evict(?CACHE, Network),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"add_resolver">>},
                                {<<"resolver">>, Resolver}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Network), <<"metadata">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    ok = ls_network:set_metadata(Network, [{Path ++ [K], V}]),
    e2qc:evict(?CACHE, Network),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State}.

%%--------------------------------------------------------------------
%% DEETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Network), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_network:set_metadata(Network, [{Path, delete}]),
    e2qc:evict(?CACHE, Network),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State};

delete(Req, State = #state{path = [?UUID(Network), <<"ipranges">>, IPRange]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_network:remove_iprange(Network, IPRange),
    e2qc:evict(?CACHE, Network),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"remove_iprange">>},
                                {<<"iprange">>, IPRange}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req, State = #state{path = [?UUID(Network), <<"resolvers">>,
                                   Resolver]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_network:remove_resolver(Network, Resolver),
    e2qc:evict(?CACHE, Network),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"remove_resolver">>},
                                {<<"resolver">>, Resolver}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req, State = #state{path = [?UUID(Network)]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_network:delete(Network),
    e2qc:evict(?CACHE, Network),
    e2qc:teardown(?LIST_CACHE),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets(<<"action">>, <<"delete">>, State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.
