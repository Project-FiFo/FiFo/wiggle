-module(wiggle_dtrace_h).
-include("wiggle.hrl").

-define(CACHE, dtrace).
-define(LIST_CACHE, dtrace_list).
-define(FULL_CACHE, dtrace_full_list).

-export([allowed_methods/3,
         init/1,
         get/1,
         permission_required/2,
         read/2,
         create/3,
         write/3,
         delete/2]).

-behaviour(wiggle_rest_h).

init(S = #state{path = [?UUID(Script)| _]}) ->
    wiggle_audit:set(<<"dtrace">>, Script, S);

init(S) ->
    S.

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>, <<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Dtrace), <<"metadata">>|_]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Dtrace)]) ->
    [<<"GET">>, <<"PUT">>, <<"DELETE">>].

get(State = #state{path = [?UUID(Dtrace) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, dtrace_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Dtrace, TTL1, TTL2, not_found,
                  fun() -> ls_dtrace:get(Dtrace) end);
            _ ->
                ls_dtrace:get(Dtrace)
        end,
    ?MSniffle(?P(State), Start),
    R;

get(_State) ->
    not_found.

permission_required(get, []) ->
    {ok, [<<"cloud">>, <<"dtraces">>, <<"list">>]};

permission_required(post, []) ->
    {ok, [<<"cloud">>, <<"dtraces">>, <<"create">>]};

permission_required(get, [?UUID(Dtrace)]) ->
    {ok, [<<"dtraces">>, Dtrace, <<"get">>]};

permission_required(delete, [?UUID(Dtrace)]) ->
    {ok, [<<"dtraces">>, Dtrace, <<"delete">>]};

permission_required(put, [?UUID(Dtrace)]) ->
    {ok, [<<"dtraces">>, Dtrace, <<"edit">>]};

permission_required(put, [?UUID(Dtrace), <<"metadata">> | _]) ->
    {ok, [<<"dtraces">>, Dtrace, <<"edit">>]};

permission_required(delete, [?UUID(Dtrace), <<"metadata">> | _]) ->
    {ok, [<<"dtraces">>, Dtrace, <<"edit">>]};

permission_required(_Method, _Path) ->
    undefined.

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------

read(Req, State = #state{path = []}) ->
    wiggle_h:list(<<"dtraces">>,
                  fun ls_dtrace:stream/3,
                  fun ft_dtrace:uuid/1,
                  fun ft_dtrace:to_json/1,
                  Req, State);

read(Req, State = #state{path = [?UUID(_Dtrace)], obj = Obj}) ->
    Obj1 = ft_dtrace:to_json(Obj),
    Obj2 = jsxd:update(<<"script">>, fun (S) ->
                                             list_to_binary(S)
                                     end, Obj1),
    State1 = wiggle_audit:sets(<<"action">>, <<"services">>, State),
    {Obj2, Req, State1}.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

create(Req, State = #state{path = [], version = Version}, Data) ->
    {ok, Dtrace} = jsxd:get(<<"name">>, Data),
    {ok, Script} = jsxd:get(<<"script">>, Data),
    Script1 = binary_to_list(Script),
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"action">>, <<"create">>},
                                {<<"name">>, Dtrace}], State),
    case ls_dtrace:add(Dtrace, Script1) of
        {ok, UUID} ->
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            ?MSniffle(?P(State), Start),
            case jsxd:get(<<"config">>, Data) of
                {ok, Config} ->
                    Start1 = erlang:system_time(micro_seconds),
                    ok = ls_dtrace:set_config(UUID, Config),
                    ?MSniffle(?P(State), Start1);
                _ ->
                    ok
            end,
            State2 = wiggle_audit:sets(<<"dtrace">>, UUID, State1),
            {{true, <<"/api/", Version/binary, "/dtrace/", UUID/binary>>}, Req,
             State2#state{body = Data}};
        duplicate ->
            State2 = wiggle_audit:fail("duplicate", State1),
            {ok, Req1} = cowboy_req:reply(409, Req),
            {halt, Req1, State2}
    end.

write(Req, State = #state{path = [?UUID(Dtrace), <<"metadata">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    ok = ls_dtrace:set_metadata(Dtrace, [{Path ++ [K], V}]),
    e2qc:evict(?CACHE, Dtrace),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State}.
%%--------------------------------------------------------------------
%% DEETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Dtrace), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_dtrace:set_metadata(Dtrace, [{Path, delete}]),
    e2qc:evict(?CACHE, Dtrace),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State};

delete(Req, State = #state{path = [?UUID(Dtrace)]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_dtrace:delete(Dtrace),
    e2qc:evict(?CACHE, Dtrace),
    e2qc:teardown(?LIST_CACHE),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets(<<"action">>, <<"delete">>, State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.
