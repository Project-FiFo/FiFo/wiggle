-module(wiggle_hypervisor_h).
-include("wiggle.hrl").

-define(CACHE, hypervisor).
-define(LIST_CACHE, hypervisor_list).
-define(FULL_CACHE, hypervisor_full_list).

-export([allowed_methods/3,
         init/1,
         permission_required/2,
         get/1,
         read/2,
         write/3,
         create/3,
         delete/2]).

-behaviour(wiggle_rest_h).

init(S = #state{path = [?UUID(Hypervisor)| _]}) ->
    wiggle_audit:set(<<"hypervisor">>, Hypervisor, S);

init(S) ->
    S.

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>];

allowed_methods(_Version, _Token, [?UUID(_Hypervisor)]) ->
    [<<"GET">>, <<"DELETE">>];

allowed_methods(_, _Token, [?UUID(_Hypervisor), <<"metrics">>| _]) ->
    [<<"GET">>];

allowed_methods(_Version, _Token, [?UUID(_Hypervisor), <<"config">>|_]) ->
    [<<"PUT">>];

allowed_methods(_Version, _Token,
                [?UUID(_Hypervisor), <<"characteristics">> | _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Hypervisor), <<"metadata">>|_]) ->
    [<<"PUT">>, <<"DELETE">>];


allowed_methods(_Version, _Token, [?UUID(_Hypervisor), <<"services">>]) ->
    [<<"PUT">>].

get(#state{path = [?UUID(_Hypervisor), <<"metrics">>]}) ->
    {ok, erlang:system_time(micro_seconds)};

get(State = #state{path = [?UUID(Hypervisor) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, hypervisor_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Hypervisor, TTL1, TTL2, not_found,
                  fun() -> ls_hypervisor:get(Hypervisor) end);
            _ ->
                ls_hypervisor:get(Hypervisor)
        end,
    ?MSniffle(?P(State), Start),
    R;

get(_State) ->
    not_found.

permission_required(get, []) ->
    {ok, [<<"cloud">>, <<"hypervisors">>, <<"list">>]};

permission_required(get, [?UUID(Hypervisor)]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"get">>]};

permission_required(get, [?UUID(Hypervisor), <<"metrics">> | _]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"get">>]};

permission_required(delete, [?UUID(Hypervisor)]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"edit">>]};

permission_required(put, [?UUID(Hypervisor), <<"config">> | _]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"edit">>]};

permission_required(put, [?UUID(Hypervisor), <<"metadata">> | _]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"edit">>]};

permission_required(delete, [?UUID(Hypervisor), <<"metadata">> | _]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"edit">>]};

permission_required(put, [?UUID(Hypervisor), <<"characteristics">> | _]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"edit">>]};

permission_required(delete, [?UUID(Hypervisor), <<"characteristics">> | _]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"edit">>]};


permission_required(put, [?UUID(Hypervisor), <<"services">>]) ->
    {ok, [<<"hypervisors">>, Hypervisor, <<"edit">>]};

permission_required(_Method, _Path) ->
    undefined.

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------
read(Req, State = #state{path = []}) ->
    wiggle_h:list(<<"hypervisors">>,
                  fun ls_hypervisor:stream/3,
                  fun ft_hypervisor:uuid/1,
                  fun ft_hypervisor:to_json/1,
                  Req, State);

read(Req, State = #state{path = [?UUID(_Hypervisor), <<"services">>, Service],
                         obj = Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"services">>, State),
    {jsxd:get([Service], #{}, ft_hypervisor:services(Obj)), Req, State1};

read(Req, State = #state{path = [?UUID(_Hypervisor)], obj = Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"get">>, State),
    {ft_hypervisor:to_json(Obj), Req, State1};

read(Req, State = #state{path = [?UUID(Hypervisor), <<"metrics">>]}) ->
    State1 = wiggle_audit:disable(State),
    {QS, Req1} = cowboy_req:qs_vals(Req),
    case perf(Hypervisor, QS) of
        {ok, JSON} ->
            {JSON, Req1, State1};
        {error, no_results} ->
            {ok, Req2} = cowboy_req:reply(503, [], <<"Empty result set">>,
                                          Req1),
            {halt, Req2, State1};
        {error, bad_qs} ->
            {ok, Req2} = cowboy_req:reply(
                           400, [], <<"bad qeruy string">>, Req1),
            {halt, Req2, State1};
        {error, bad_resolution} ->
            {ok, Req2} = cowboy_req:reply(400, [], <<"bad resolution">>, Req1),
            {halt, Req2, State1}
    end.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

create(Req, State, _Data) ->
    {halt, Req, State}.

write(Req, State = #state{path = [?UUID(Hypervisor), <<"config">>]},
      #{<<"alias">> := V}) when is_binary(V) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_hypervisor:alias(Hypervisor, V),
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"set_alias">>},
                                {<<"alias">>, V}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Hypervisor), <<"config">>]},
      #{<<"path">> := P}) when is_list(P) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_hypervisor:path(Hypervisor, path_to_erl(P)),
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"set_path">>},
                                {<<"path">>, P}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req,
      State = #state{path = [?UUID(Hypervisor), <<"characteristics">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    FullPath = Path ++ [K],
    Reply = ls_hypervisor:set_characteristic(Hypervisor, [{FullPath, V}]),
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"set_characteristic">>},
                                {<<"path">>, FullPath},
                                {<<"value">>, V}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Hypervisor), <<"metadata">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    ls_hypervisor:set_metadata(Hypervisor, [{Path ++ [K], V}]),
    ?MSniffle(?P(State), Start),
    {true, Req, State};

write(Req, State = #state{path = [?UUID(Hypervisor), <<"services">>]},
      #{<<"action">> := <<"enable">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_hypervisor:service_action(Hypervisor, enable, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"enable">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Hypervisor), <<"services">>]},
      #{<<"action">> := <<"disable">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_hypervisor:service_action(Hypervisor, disable, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"disable">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Hypervisor), <<"services">>]},
      #{<<"action">> := <<"clear">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_hypervisor:service_action(Hypervisor, clear, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"clear">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Hypervisor), <<"services">>]},
      #{<<"action">> := <<"refresh">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_hypervisor:service_action(Hypervisor, refresh, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"refresh">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Hypervisor), <<"services">>]},
      #{<<"action">> := <<"restart">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_hypervisor:service_action(Hypervisor, restart, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"restart">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.

%%--------------------------------------------------------------------
%% DELETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Hypervisor)]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_hypervisor:unregister(Hypervisor),
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    e2qc:teardown(?LIST_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets(<<"action">>, <<"delete">>, State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req, State = #state{path = [?UUID(Hypervisor), <<"characteristics">>
                                       | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_hypervisor:set_characteristic(Hypervisor, [{Path, delete}]),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"remove_characteristic">>},
                                {<<"characteristic">>, Path}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req,
       State = #state{path = [?UUID(Hypervisor), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    e2qc:evict(?CACHE, Hypervisor),
    e2qc:teardown(?FULL_CACHE),
    ls_hypervisor:set_metadata(Hypervisor, [{Path, delete}]),
    ?MSniffle(?P(State), Start),
    {true, Req, State}.

%%--------------------------------------------------------------------
%% Internal
%%--------------------------------------------------------------------
path_to_erl(P) ->
    [{N, C} || #{<<"cost">> := C, <<"name">> := N} <- P,
               is_integer(C), is_binary(N), N /= <<>>].


perf(Hv, QS) ->
    Elems = perf_cpu(Hv),
    wiggle_metrics:get(Elems, QS).

perf_cpu(Hv) ->
    [{"cpu-kernel",  cpu(Hv, kernel)},
     {"cpu-idle",    cpu(Hv, idle)},
     {"cpu-user",    cpu(Hv, user)}].

h(L) ->
    Bkt = application:get_env(wiggle, server_bucket, server),
    {m, Bkt, L}.

cpu(Hv, Metric) ->
    {f, derivate, [{f, sum, [h([Hv, cpu, "*", Metric])]}]}.
